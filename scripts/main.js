require(['jquery',
         'lib/gameloop',
         'lib/mokey',
         'lib/gridpartition',
         'app/game',
         'app/scene',
         'app/entity/creature',
         'app/renderer',
         'app/hitbox',
],
function($, GameLoop, Mokey, Grid, Game, Scene, Creature, Renderer, Hitbox) {

  var game      = new Game();
  var scene     = new Scene(new Grid([1500, 1400], [100, 100]));
  var renderer  = new Renderer($('#screen')[0]);

  var player = new Creature();
  player.setPosition([200, 200]);
  player.stats({
    hp:     100,
    mp:     10,
    str:    10,
    dex:    20,
    speed:  200,
    mass:   1
  });
  player.addHitbox(new Hitbox(player, {
    top:    -50.0,
    right:   12.5,
    bottom:   0.0,
    left:   -12.5
  }));

window.player = player;

  scene.addEntity(player);
  game.pushScene(scene);
  //player.renderWith(new Renderer.div('<div id="player">'));

  var update = function(dt) {
    game.update(dt);
  };

  var render = function(alpha) {
    renderer.render(game.focusedScene(), alpha);
  };

  var loop = new GameLoop(update, render);

  // Frames Per Second Display
  var fps = document.getElementById('fps');
  setInterval(function(){
    fps.innerHTML = loop.fps();
  }, 1000);

  // Keys
  Mokey.on('e r t', function(){
    console.log('ERT!');
  });

  Mokey.on('w', function(){
    player.steerOn(player.NORTH);
  }).on('s', function(){
    player.steerOn(player.SOUTH);
  }).on('a', function(){
    player.steerOn(player.WEST);
  }).on('d', function(){
    player.steerOn(player.EAST);
  });

  Mokey.on('keyup.w', function(){
    player.steerOff(player.NORTH);
  }).on('keyup.s', function(){
    player.steerOff(player.SOUTH);
  }).on('keyup.a', function(){
    player.steerOff(player.WEST);
  }).on('keyup.d', function(){
    player.steerOff(player.EAST);
  });

  Mokey.on('aX2', function(){
    player.steerOn(player.WEST);
    player.performActivity('run');
  }).on('dX2', function(){
    player.steerOn(player.EAST);
    player.performActivity('run');
  });

  Mokey.on('space', function(){
    player.performActivity('jump');
  });

  Mokey.on('k', function() {
    player.performActivity('slash');
  });

  Mokey.on('l', function() {
    player.performActivity('thrust');
  });

  loop.start();

});

