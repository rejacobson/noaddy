(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.GameLoop = factory();
    }
}(this, function() {

  var requestAnimationFrame = (function(){
    //Check for each browser
    //@paul_irish function
    //Globalises this function to work on any browser as each browser has a different namespace for this
    try{
      window;
    } catch(e){
      return;
    }

    return window.requestAnimationFrame ||        //Chromium
           window.webkitRequestAnimationFrame ||  //Webkit
           window.mozRequestAnimationFrame ||     //Mozilla Geko
           window.oRequestAnimationFrame ||       //Opera Presto
           window.msRequestAnimationFrame ||      //IE Trident?
           function(callback, element) {          //Fallback function
             window.setTimeout(callback, 1000/60);
           }
  })();

  var pageHiddenProperty = (function(){
    var prefixes = ['webkit', 'moz', 'ms', 'o'];

    // if 'hidden' is natively supported just return it
    if ('hidden' in document) return 'hidden';

    // otherwise loop over all the known prefixes until we find one
    for (var i=0; i<prefixes.length; i++) {
      if ((prefixes[i] + 'Hidden') in document) return prefixes[i] + 'Hidden';
    }

    // otherwise it's not supported
    return null;
  })();

  function pageIsHidden() {
    if (!pageHiddenProperty) return false;
    return document[pageHiddenProperty];
  };

  function isFunction(functionToCheck) {
   var getType = {};
   return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
  }

  function onVisibilityChange(hidden_callback, visible_callback) {
    if (pageHiddenProperty) {
      var eventname = pageHiddenProperty.replace(/[H|h]idden/, '') + 'visibilitychange';
      document.addEventListener(eventname, function(event){
        if (pageIsHidden()) {
          if (hidden_callback && isFunction(hidden_callback)) hidden_callback(event);
        } else {
          if (visible_callback && isFunction(visible_callback)) visible_callback(event);
        }
      });
    }
  };

  var GameLoop = function(update_callback, render_callback) {
    if (!update_callback) throw('No update callback provided');

    var paused      = false;

    var t           = 0.0;
    var dt          = 1/60;  // 60 fps
    var frameTime;

    var currentTime = performance.now();
    var accumulator = 0.0;

    var fps         = 0;
    var frames      = 0;
    var old_fps_t   = 0;

    function tick(newTime) {
      frameTime = (newTime - currentTime) * 0.001;
      if (frameTime > 0.25) frameTime = 0.25;
      currentTime = newTime;

      accumulator += frameTime;

      while (accumulator >= dt) {
        update_callback(dt);
        t += dt;
        accumulator -= dt;
      }

      frames++;
      if (newTime > old_fps_t + 1000) {
        fps       = frames;
        frames    = 0;
        old_fps_t = newTime;
      }

      if (render_callback) render_callback(accumulator / dt);

      if (!paused) requestAnimationFrame(tick);
    }

    this.start = function() {
      currentTime = performance.now();
      fps = frames = old_fps_t = 0;
      requestAnimationFrame(tick);
    }

    this.pause = function() {
      paused = true;
    }

    this.unpause = function() {
      paused = false;
      this.start();
    }

    this.fps = function() {
      return fps;
    }

    var self = this;
    onVisibilityChange(function(){
      self.pause();
    }, function(){
      self.unpause();
    });
  };

  return GameLoop;

}));
