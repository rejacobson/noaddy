(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['lodash'], factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory(require('lodash'));
    } else {
        // Browser globals (root is window)
        root.GridPartition = factory(root._);
    }
}(this, function (_) {

  //                  [w, h]   [w, h]
  var Grid = function(mapsize, cellsize) {
    if (!_.isArray(mapsize)  || mapsize.length != 2)   throw new Error('EntityList map_size must be a 2 element array: [map_w, map_h]');
    if (!_.isArray(cellsize) || cellsize.length != 2)  throw new Error('EntityList cells must be a 2 element array: [cell_w, cell_h]');

    // The number of cells in each of the x and y axis
    var cell_count_x = Math.round(mapsize[0] / cellsize[0]);
    var cell_count_y = Math.round(mapsize[1] / cellsize[1]);

    // The size of the area being partitioned.
    this.mapsize = mapsize;

    // The number of cells in each of the x and y axis
    this.cellcount = [cell_count_x, cell_count_y];

    // Recalculate the cell sizes based off of the x and y cell counts
    this.cellsize = [mapsize[0] / cell_count_x, mapsize[1] / cell_count_y];

    // The largest cell index in the map
    this.maxindex = this.cellcount[0] * this.cellcount[1] - 1;

    // Keep track of the number of elements in each cell
    // _residents[cell_index] == number of elements
    this.residents = {};

    // Array of grid cells
    // This holds the entities/values being partitioned
    this.map = [];

    this.length = 0;
  };

  Grid.prototype = {
    outOfBounds: function(index_or_position) {
      return _.isArray(index_or_position)
        ? this.positionOutOfBounds(index_or_position)
        : this.indexOutOfBounds(index_or_position);
    },

    indexOutOfBounds: function(index) {
      return index < 0 || index > this.maxindex;
    },

    positionOutOfBounds: function(position) {
      return position[0] < 0 ||
             position[0] > this.mapsize[0] - 1 ||
             position[1] < 0 ||
             position[1] > this.mapsize[1] - 1;
    },

    // Get a cell x and y coordinates by a cell index
    indexToPosition: function(index) {
      if ('number' != typeof index) throw('#indexToPosition - index is not a number: '+ index);
      return [index % this.cellcount[0], Math.floor(index / this.cellcount[0])];
    },

    positionToIndex: function(position) {
      if (!_.isArray(position)) throw('#positionToIndex - position is not an array: '+ position);
      if (this.positionOutOfBounds(position)) return null;
      var x = Math.floor(position[0] / this.cellsize[0]);
      var y = Math.floor(position[1] / this.cellsize[1]);
      return (y * this.cellcount[0]) + x;
    },

    // Return the stored element counts.
    // Only cells with more than one element in them are represented.
    getResidents: function() {
      return this.residents;
    },

    // Add a new element to the grid, in the specified cell index
    insert: function(index, value) {
      var cell = this.getCellByIndex(index);
      if (!cell) return;
      cell.push(value);
      this.length++;
      if (!this.residents[index]) this.residents[index] = 0;
      this.residents[index]++; // increment the number of elements in this cell
    },

    // Remove an element from the grid and return it
    remove: function(index, value) {
      var cell = this.getCellByIndex(index);
      if (!cell) return;

      for (var i=0; i<cell.length; ++i) {
        if (cell[i] == value) {
          cell.splice(i, 1);
          this.length--;
          this.residents[index]--;
          if (this.residents[index] <= 0) delete this.residents[index];
          break;
        }
      }

      return value;
    },

    getCellByIndex: function(index) {
      if (this.indexOutOfBounds(index)) return;
      return this.map[index] || (this.map[index] = []);
    },

    getCellByPosition: function(position) {
      return this.getCellByIndex(this.positionToIndex(position));
    },

    getCell: function(index_or_position) {
      return _.isArray(index_or_position)
        ? this.getCellByPosition(index_or_position)
        : this.getCellByIndex(index_or_position);
    },

    getCells: function(indices_or_positions) {
      var cells = [];
      var len   = indices_or_positions.length;
      for (var i = 0; i < len; i++) {
        cells = cells.concat(this.getCell(indices_or_positions[i]));
      };
      return cells;
    },

    // Get the contents of all cells surrounding the cell at index
    getSurroundingCells: function(index_or_position) {
      return this.getCells(this.getSurroundingIndices(index_or_position));
    },

    // Get a list of all indices surrounding the cell at index
    getSurroundingIndices: function(index_or_position) {
      if (_.isArray(index_or_position)) {
        var index     = this.positionToIndex(index_or_position);
        var position  = index_or_position;
      } else {
        var index     = index_or_position;
        var position  = this.indexToPosition(index_or_position);
      }

      var indices = [];
      var x       = position[0];
      var y       = position[1];
      var width   = this.cellcount[0];
      var height  = this.cellcount[1];

      // X is not touching left hand side
      if (x > 0) {
        indices.push(index - 1); // WEST

        if (y > 0)          indices.push(index - width - 1); // NORTH WEST
        if (y < height - 1) indices.push(index + width - 1); // SOUTH WEST
      }

      // X is not touching the right hand side
      if (x < width - 1) {
        indices.push(index + 1); // EAST

        if (y > 0)          indices.push(index - width + 1); // NORTH EAST
        if (y < height - 1) indices.push(index + width + 1); // SOUTH EAST
      }

      // Y is not touching the top
      if (y > 0)            indices.push(index - width); // NORTH

      // Y is not touching the bottom
      if (y < height - 1)   indices.push(index + width); // SOUTH

      return indices;
    }
  };

  return Grid;

}));
