define(['lib/entitylist',
        'lib/vector2d'
],
function(EntityList, V) {

  var Scene = function(partition) {
    this.entitylist = new EntityList(partition);
  };

  Scene.prototype = {
    entities: function() {
      return this.entitylist.entities;
    },

    update: function(dt) {
      this.entitylist.update(dt, this);
    },

    addEntity: function(entity) {
      this.entitylist.insert(entity);
    },

    removeEntity: function() {
      this.entitylist.remove(entity);
    },

    entitylistNear: function(position) {
      return this.entitylist.partition.getSurroundingCells(position);
    },

    entitylistAt: function(position) {
      return this.entitylist.partition.getCells(position);
    },

    entitylistBetween: function(p1, p2) {
      var cellsize  = this.entitylist.partition.cellsize;
      var line      = V.divide(V.subtract(V.clone(p2), p1), cellsize);
      var start     = V.clone(p1);
    }
  };

  return Scene;

});
