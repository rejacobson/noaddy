define(['app/activities/all'], function(Activities) {

  var Activity = function(entity) {
    this.entity = entity;
    this.timer  = 0;
    this.name;
    this.activity;
  };

  Activity.prototype = {
    isBusy: function() {
      return this.timer > 0;
    },

    isEmpty: function() {
      return !this.activity;
    },

    load: function(name) {
      if (this.isBusy()) return;
      this.name     = name;
      this.activity = Activities[name];
      this.timer    = 0;
      this.run('start');
    },

    unload: function() {
      if (!this.activity) return;
      this.run('stop');
      this.name     = null;
      this.activity = null;
      this.timer    = 0;
    },

    run: function(func, dt) {
      if (!this.activity[func]) return;
      return this.activity[func].call(this, this.entity, dt);
    },

    update: function(dt) {
      if (!this.activity) return;
      if (this.activity['time']) {
        if (this.timer > this.activity['time']) return this.unload();
        this.timer += dt;
      }
      this.run('update', dt);
      if (this.run('cancel')) return this.unload();
    }
  };

  return Activity;

});
