define(['jquery'],
function($){

  var Renderer = function(canvas) {
    this.canvas = canvas;
    this.ctx    = canvas.getContext('2d');
  };

  Renderer.prototype = {
    clear: function() {
      // Store the current transformation matrix
      this.ctx.save();

      // Use the identity matrix while clearing the canvas
      this.ctx.setTransform(1, 0, 0, 1, 0, 0);
      this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

      // Restore the transform
      this.ctx.restore();
    },

    render: function(scene, alpha) {
      this.clear();

      var rect;
      var entities = scene.entities();
      var hitboxes;
      var e;

      for (var i=0, len=entities.length; i<len; i++) {
        e         = entities[i];
        hitboxes  = e.hitboxes();

        this.ctx.beginPath();
        this.ctx.strokeStyle = '#ddd';

        for (var j=0, jlen=hitboxes.length; j<jlen; j++) {
          rect = hitboxes[j].getRect([
            lerp(e._p[0], e.old._p[0], alpha),
            lerp(e._p[1], e.old._p[1], alpha),
            lerp(e._p[2], e.old._p[2], alpha)
          ]);

          this.ctx.rect((0.5 + rect.left) | 0,
                        (0.5 + rect.top) | 0,
                        (0.5 + rect.width) | 0,
                        (0.5 + rect.height) | 0);
        }

        this.ctx.stroke();
      }
    }
  };

  var lerp = function(current, previous, alpha) {
    return current * alpha + previous * (1.0 - alpha);
  };

  return Renderer;

});
