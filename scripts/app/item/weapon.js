define(
function() {

  return {
    shortsword: {
      hp: -10,
      mass: 2,
      speed: 0.5,
      length: 2
    },

    longsword: {
      hp: -15,
      mass: 3,
      speed: 0.4,
      length: 3
    },

    spear: {
      hp: -15,
      mass: 4,
      speed: 0.3,
      length: 7
    }
  };

});


