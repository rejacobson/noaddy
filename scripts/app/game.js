define(function() {

  var Game = function() {
    this.scenes = [];
  };

  Game.prototype = {
    focusedScene: function() {
      return this.scenes[0];
    },

    update: function(dt) {
      var scene;
      for (var i=0; (scene = this.scenes[i]); i++) {
        scene.update(dt);
      }
    },

    pushScene: function(scene) {
      this.scenes.push(scene);
    },

    popScene: function() {
      this.scenes.pop();
    }
  };

  return Game;

});

