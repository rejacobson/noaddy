define(['lib/vector3d', 'app/hitbox'],
function(V, Hitbox) {

  return {
    'slash': {
      time: 0.1,

      start: function(entity) {
        var facing = entity._facing[0];

        entity._attack_box = new Hitbox(entity, {
          top:    -45,
          right:   facing == -1 ? -20 : 60,
          bottom: -5,
          left:    facing == -1 ? -60 : 20,
        });

        entity.addHitbox(entity._attack_box);
      },

      stop: function(entity) {
        entity.removeHitbox(entity._attack_box);
        entity._attack_box = undefined;
      },

      cancel: function(entity) {
      },

      update: function(entity, dt) {
        if (entity.onGround()) V.scale(entity._v, 0.85);
      }
    },

    'thrust': {
      time: 0.5,

      start: function(entity) {
        var facing = entity._facing[0];

        entity._attack_box = new Hitbox(entity, {
          top:    -40,
          right:   facing == -1 ? -20 : 90,
          bottom: -20,
          left:    facing == -1 ? -90 : 20,
        });

        entity.addHitbox(entity._attack_box);
        entity.addForce(15000 * facing, 0);
      },

      stop: function(entity) {
        entity.removeHitbox(entity._attack_box);
        entity._attack_box = undefined;
      },

      cancel: function(entity) {
      },

      update: function(entity, dt) {
        if (this.timer > 0.25 && entity.onGround()) {
          V.scale(entity._v, 0.85);
        }
      }
    },

    'bite': {
      time: 5,

      update: function(entity, dt) {

      }
    }
  };

});


