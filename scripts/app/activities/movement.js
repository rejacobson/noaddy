define(['lib/vector3d'],
function(V) {

  FRICTION = 0.75;

  return {
    'idle': {
      update: function(entity, dt) {
        var m = entity._moving;
        if (m[0] != 0 || m[1] != 0) entity.performActivity('walk');
      },
    },

    'walk': {
      cancel: function(entity) {
        var m = entity._moving;
        return m[0] == 0 && m[1] == 0;
      },

      update: function(entity, dt) {
        if (!entity.onGround()) return;

        var m     = entity._moving;
        var v     = entity.velocity();
        var dex   = entity.stat('dex');
        var speed = entity.stat('speed');

        entity.addForce(v[0] * m[0] > speed          ? 0 : speed * m[0] * FRICTION * dex * 0.25,
                        v[1] * m[1] > speed * 0.75   ? 0 : speed * m[1] * FRICTION * dex * 0.25);
      }
    },

    'run': {
      cancel: function(entity) {
        var m = entity._moving;
        return m[0] == 0;
      },

      update: function(entity, dt) {
        if (!entity.onGround()) return;

        var m     = entity._moving;
        var v     = entity.velocity();
        var dex   = entity.stat('dex');
        var speed = entity.stat('speed') * 2;

        entity.addForce(v[0] * m[0] > speed          ? 0 : speed * m[0] * FRICTION * dex * 0.25,
                        v[1] * m[1] > speed * 0.1    ? 0 : speed * m[1] * FRICTION * dex * 0.1);
      }
    },

    'jump': {
      cancel: function(entity) { return true; },

      update: function(entity, dt) {
        if (entity.onGround()) entity.addForce(0, 0, entity.stat('dex') * 100000 * dt);
      }
    },

    'fall': {
      update: function(entity, dt) {
      }
    },

    'getup': {
      time: 2,

      update: function(entity, dt) {
      }
    }
  };

});

