define(['lodash',
        'app/activities/attack',
        'app/activities/movement'
],
function(_, Attack, Movement) {

  return _.merge({}, Attack, Movement);

});
