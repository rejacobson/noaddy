define(['lodash'],
function(_) {

  var Hitbox = function(entity, opts) {
    this.entity = entity;
    this.active = true;
    this.rect   = {
      top:    opts.top    || 0,
      bottom: opts.bottom || 0,
      left:   opts.left   || 0,
      right:  opts.right  || 0
    };
    this._sendHit     = opts.sendHit    || function() {};
    this._receiveHit  = opts.receiveHit || function() {};
  };

  Hitbox.prototype = {
    activate: function() {
      this.active = true;
    },

    deactivate: function() {
      this.active = false;
    },

    getRect: function(position) {
      var p = position || this.entity.position();
      return {
        top:    p[1] - p[2] + this.rect.top,
        bottom: p[1] - p[2] + this.rect.bottom,
        left:   p[0] + this.rect.left,
        right:  p[0] + this.rect.right,
        width:  this.rect.right - this.rect.left,
        height: this.rect.bottom - this.rect.top
      };
    },

    sendHit: function(other_entity) {
      return this._sendHit(other_entity);
    },

    receiveHit: function(other_entity, response) {
      var effect = this._receiveHit(other_entity, response);
      if (effect) this.applyStats(effect);
    },

    applyStats: function(stats) {
      for (var key in response) {
        if (_.isFunction(entity[key])) entity[key](response[key]);
      }
    }
  };

  Hitbox.overlaps = function(rect1, rect2) {
    return !(rect1.right  < rect2.left  ||
             rect1.left   > rect2.right ||
             rect1.bottom < rect2.top   ||
             rect1.top    > rect2.bottom);
  };

  return Hitbox;

});
