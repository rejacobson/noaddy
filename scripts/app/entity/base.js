define(['lodash',
        'lib/inheritance',
        'lib/vector3d',
        'app/entity/mixins/stats',
        'app/entity/mixins/physics',
        'app/entity/mixins/activity',
        'app/entity/mixins/hitbox'], //, 'app/mixins/tags', 'app/mixins/avatar'],
function(_, Class, V, Stats, Physics, Activity, Hitbox){ //, Tags, Stats, Avatar, Rendering

  var __id = 0;

  var Entity = Class.extend({

    include: [Stats, Physics, Activity, Hitbox],

    initialize: function() {
      if (this.init) this.init();

      var _id = ++__id;
      this.id = function() { return _id; }
    }

  });

  return Entity;

});
