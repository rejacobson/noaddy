define(['lib/inheritance',
        'app/entity/base',
        'app/entity/mixins/steerable'],
function(Class, Base, Steerable) {

  var Creature = Class.extend(Base, {

    include: [Steerable],

    initialize: function() {
      this.parent();
    }
  });

  return Creature;

});
