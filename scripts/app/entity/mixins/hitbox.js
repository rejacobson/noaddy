define(function() {

  return {
    init: function() {
      this._hitboxes = [];
    },

    addHitbox: function(hitbox) {
      this.removeHitbox(hitbox);
      this._hitboxes.push(hitbox);
    },

    removeHitbox: function(hitbox) {
      var index = this._hitboxes.indexOf(hitbox);
      if (index >= 0) this._hitboxes.splice(index, 1);
    },

    hitboxes: function() {
      return this._hitboxes;
    }
  };

});
