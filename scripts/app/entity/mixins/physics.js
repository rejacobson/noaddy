define(['lodash',
        'lib/inheritance',
        'lib/vector3d'],
function(_, Class, V) {

  return {
    init: function() {
      this._p = [0, 0, 0]; // [x, y, altitude]
      this._v = [0, 0, 0];
      this._f = [0, 0, 0];

      this.old = {
        _p: [0, 0, 0]
      };
    },

    addForce: function(x, y, z) {
      this._f[0] += x || 0;
      this._f[1] += y || 0;
      this._f[2] += z || 0;
    },

    position: function() {
      return V.clone(this._p);
    },

    setPosition: function(p) {
      this.old._p[0] = p[0];
      this.old._p[1] = p[1];
      this.old._p[2] = p[2] != undefined ? p[2] : this.old._p[2];

      this._p[0] = p[0];
      this._p[1] = p[1];
      this._p[2] = p[2] != undefined ? p[2] : this._p[2];
    },

    velocity: function() {
      return V.clone(this._v);
    },

    setVelocity: function(v) {
      this._v[0] = v[0];
      this._v[1] = v[1];
      this._v[2] = v[2] != undefined ? v[2] : this._v[2];
    },

    has_moved: function() {
      return (this.old._p[0] != this._p[0] ||
              this.old._p[1] != this._p[1] ||
              this.old._p[2] != this._p[2]);
    },

    onGround: function() {
      return this._p[2] <= 0;
    },

    update: function(dt) {
      // Gravity
      if (!this.onGround()) {
        this.addForce(0, 0, -3000);
      }

      V.scale(this._f, dt);

      // Apply velocity to the position
      if (V.lengthOfSq(this._v) != 0) {
        this.old._p[0] = this._p[0];
        this.old._p[1] = this._p[1];
        this.old._p[2] = this._p[2];

        this._p[0] += this._v[0] * dt;
        this._p[1] += this._v[1] * 0.75 * dt;
        this._p[2] += this._v[2] * dt;
      }

      this._v[0] = Math.round((this._v[0] + this._f[0]) * 100) * 0.01;
      this._v[1] = Math.round((this._v[1] + this._f[1]) * 100) * 0.01;
      this._v[2] = Math.round((this._v[2] + this._f[2]) * 100) * 0.01;

      V.set(this._f, 0, 0, 0);

      // Friction
      if (this.onGround()) {
        if (this._moving[0] == 0 || this._moving[0] * this._v[0] < 0) this._v[0] *= 0.85;
        if (this._moving[1] == 0 || this._moving[1] * this._v[1] < 0) this._v[1] *= 0.85;
      }

      // Constraints
      if (this._p[2] < 0) {
        this._p[2] = 0;
        this._v[2] = 0;
      }
    }

  };

});

