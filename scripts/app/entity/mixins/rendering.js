define(['jquery'], function($) {

  return {
    renderWith: function(renderer) {
      this._renderer = renderer;
    },

    render: function() {
      this._renderer.call(this);
    }
  };

});
