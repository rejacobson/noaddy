define(function() {

  var setDirection = function(prop, dir) {
    prop[0] = dir[0] || prop[0];
    prop[1] = dir[1] || prop[1];
    return prop;
  }

  var unsetDirection = function(prop, dir) {
    prop[0] = (prop[0] * dir[0]) > 0 ? 0 : prop[0];
    prop[1] = (prop[1] * dir[1]) > 0 ? 0 : prop[1];
    return prop;
  }

  return {
    NORTH:  Object.freeze([ 0, -1]),
    SOUTH:  Object.freeze([ 0,  1]),
    WEST:   Object.freeze([-1,  0]),
    EAST:   Object.freeze([ 1,  0]),

    init: function() {
      this._moving          = [0, 0];
      this._facing          = [1, 0];
      this._steering_locked = false;
    },

    lockSteering: function() {
      this._steering_locked = true;
    },

    unlockSteering: function() {
      this._steering_locked = false;
    },

    stopSteering: function() {
      this._moving[0] = 0;
      this._moving[1] = 0;
    },

    steerOn: function(dir) {
      if (this._steering_locked) return;
      setDirection(this._moving, dir);
      setDirection(this._facing, dir);
    },

    steerOff: function(dir) {
      if (this._steering_locked) return;
      unsetDirection(this._moving, dir);
    }
  };

});
