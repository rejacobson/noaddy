define(['app/activity'],
function(Activity) {

  return {
    init: function() {
      this._activity = new Activity(this);
    },

    cancelActivity: function() {
      this._activity.unload();
    },

    performActivity: function(name) {
      this._activity.load(name);
    },

    update: function(dt) {
      if (this._activity.isEmpty()) this._activity.load('idle');
      this._activity.update(dt);
    }
  };

});


