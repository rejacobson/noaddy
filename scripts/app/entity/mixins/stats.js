define(['lodash'], function(_) {

  return {
    init: function() {
      this._stats = {
        base: {},
        raw:  {}
      };
    },

    // Get or set all of the stats
    stats: function(def) {
      if (_.isObject(def)) {
        _.assign(this._stats['base'], def);
        this.resetStats();
      }
      return this._stats['raw'];
    },

    // Get or set a single stat
    stat: function(name, value) {
      if (value !== undefined) this._stats['raw'][name] = value;
      return this._stats['raw'][name];
    },

    // Get the base stats; read-only
    baseStats: function() {
      return _.clone(this._stats['base']);
    },

    // Get a single base stat
    baseStat: function(name) {
      return this._stats['base'][name];
    },

    // Reset all of the stats to the base_stats
    resetStats: function() {
      this._stats['raw'] = _.clone(this._stats['base']);
    },

    // Reset one stat back to it's base_stat
    resetStat: function(name) {
      this._stats['raw'][name] = this._stats['base'][name];
    },

    // All current stats become the new base_stats
    saveStats: function() {
      this._stats['base'] = _.clone(this._stats['raw']);
    },

    // One stat becomes the new base_stat
    saveStat: function(name) {
      this._stats['base'][name] = this._stats['raw'][name];
    }
  };

});
